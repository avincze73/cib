package com.cib;

import com.cib.intf.LogServicesRemoteInterface;
import com.cib.wsclient.EchoCxfService;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.ws.WebServiceRef;
import java.util.Properties;

@Stateless(name = "RemoteEjbClientServicesEJB")
@LocalBean
public class RemoteEjbClientServices {


    @WebServiceRef()
    private EchoCxfService service;

    public String callRemoteEjb(){
        //Properties prop = new Properties();
        //prop.put(Context.PROVIDER_URL, "iiop:localhost:2809");
//        Object found = null;
//        try {
//            found = new InitialContext().lookup("corbaname::dev1.exclusivenet.hu:9100#ejb/global/cib-server4-ear_ear_exploded/cib-server4-ejb-1.0-SNAPSHOT/LogServicesEJB");
//        } catch (NamingException e) {
//            e.printStackTrace();
//        }
//        LogServicesRemoteInterface remote = (LogServicesRemoteInterface) found;
//        remote.log();
        String ret = service.getLibrary().echo();
        return ret;
    }

}
