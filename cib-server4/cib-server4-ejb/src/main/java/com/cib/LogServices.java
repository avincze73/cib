package com.cib;

import com.cib.intf.LogServicesRemoteInterface;

import javax.ejb.Stateless;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless(name = "LogServicesEJB")
public class LogServices implements LogServicesRemoteInterface {

    @Override
    public void log() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "LogServices.log is called.");

    }
}
