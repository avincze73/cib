package com.cib.server4.cxf;

import javax.jws.WebService;

@WebService
public interface EchoInterface {
    String echo();
}
