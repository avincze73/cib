package com.cib.server4.cxf;

import javax.jws.WebService;

@WebService(endpointInterface="com.cib.server4.cxf.EchoInterface",
        portName="library", targetNamespace="http://com.cib.server4.cxf.Echo/library/wsdl",
        serviceName="EchoCxfService")
public class EchoCxfService implements EchoInterface {
    @Override
    public String echo() {

        return "echo from EchoCxfService";
    }
}
