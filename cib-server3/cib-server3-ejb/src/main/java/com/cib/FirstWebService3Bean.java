package com.cib;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless(name = "FirstWebService3EJB")
@WebService(name = "FirstWebService3EJB", serviceName = "FirstWebService3EJB",
        portName = "FirstWebService3EJBPortName")

public class FirstWebService3Bean {

    @WebMethod
    public String echo(){
        return "Echo from FirstWebService3EJB  1";
    }

}
