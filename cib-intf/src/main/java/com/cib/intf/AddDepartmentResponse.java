package com.cib.intf;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.rpc.server.ServiceLifecycle;
import java.io.Serializable;


@XmlRootElement
@XmlType(name = "AddDepartmentOutputMessage")
public class AddDepartmentResponse implements Serializable, Cloneable{
    private int responseCode;
    private String responseMessage;

    public AddDepartmentResponse() {
    }

    public AddDepartmentResponse(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
