package com.cib.intf;


import javax.ejb.Local;
import javax.ejb.Remote;

@Remote
public interface LogServicesRemoteInterface {
    void log();
}
