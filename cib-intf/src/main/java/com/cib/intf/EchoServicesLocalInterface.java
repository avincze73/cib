package com.cib.intf;


import javax.ejb.Local;

@Local
public interface EchoServicesLocalInterface {
    void echo();
}
