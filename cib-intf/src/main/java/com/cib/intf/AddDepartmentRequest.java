package com.cib.intf;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement
@XmlType(name = "AddDepartmentInputMessage")
public class AddDepartmentRequest implements Serializable, Cloneable {
    private String name;

    public AddDepartmentRequest(String name) {
        this.name = name;
    }

    public AddDepartmentRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
