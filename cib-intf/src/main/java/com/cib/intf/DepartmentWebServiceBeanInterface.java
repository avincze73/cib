package com.cib.intf;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface DepartmentWebServiceBeanInterface {
    @WebMethod
    void echo();
}
