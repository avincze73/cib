package com.cib.server5.web;

import com.cib.RemoteEjbClientServices;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "RemoteEjbClientServlet", urlPatterns = {"remoteejbclientservlet"})
public class RemoteEjbClientServlet extends HttpServlet {


    @EJB
    private RemoteEjbClientServices remoteEjbClientServices;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger.getLogger(getClass().getName()).log(Level.INFO, remoteEjbClientServices.callRemoteEjb());

    }
}
