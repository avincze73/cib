/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib;

import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author martin
 */
@Stateless(name = "jndibean", mappedName = "jndimappedname")
@LocalBean
public class JndiSessionBean {

    public void businessMethod() {
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

}
