package com.cib;

import javax.ejb.Stateless;

@Stateless(name = "RemoteEjbClientServiceEJB")
public class RemoteEjbClientService {

    public String callRemoteEjb(){
        return "ok";
    }

}
