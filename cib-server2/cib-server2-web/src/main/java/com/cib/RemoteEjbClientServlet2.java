package com.cib;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "RemoteEjbClientServlet2", urlPatterns = {"remoteejbclientservlet2"})
public class RemoteEjbClientServlet2 extends HttpServlet {


    @EJB
    private RemoteEjbClientService remoteEjbClientService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger.getLogger(getClass().getName()).log(Level.INFO, remoteEjbClientService.callRemoteEjb());

    }
}
