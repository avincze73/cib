package com.cib.server1.producers;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CibProducer {

    @Produces
    @PersistenceContext(unitName="cib-server1-pu")
    @CibDatabase
    EntityManager em;

    @Produces
    @PersistenceContext(unitName="cib-server1-pu2")
    @Cib2Database
    EntityManager em2;

}
