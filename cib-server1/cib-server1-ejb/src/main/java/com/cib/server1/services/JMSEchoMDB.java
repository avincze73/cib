package com.cib.server1.services;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Level;
import java.util.logging.Logger;

@MessageDriven(
        name = "JMSEchoMDB",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationType",
                        propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination",
                        propertyValue = "jndi_INPUT_Q")
        }
)
public class JMSEchoMDB implements MessageListener {

    @Override
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void onMessage(Message message) {
        TextMessage textMsg = (TextMessage) message;
        try {
            String text = textMsg.getText();
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Received message: " + text);
        } catch (JMSException e) {
            e.printStackTrace();
        }


    }
}
