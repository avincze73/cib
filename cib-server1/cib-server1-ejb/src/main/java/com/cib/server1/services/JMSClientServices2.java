package com.cib.server1.services;

import com.cib.server1.interceptors.LogInterceptor;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class JMSClientServices2 {

    @Inject
    @JMSConnectionFactory( "jms/libertyQCF")
    private JMSContext jmsContext;

    @Resource(lookup = "jndi_INPUT_Q")
    private Queue queue;

    public void sendMessageToQueue() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Sending third message to jms queue");
        String text = "third message from jms client";
        jmsContext.createProducer().send(queue, text);
    }
}
