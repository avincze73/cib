/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import com.cib.intf.EchoServicesLocalInterface;
import com.cib.server1.interceptors.LogInterceptor;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.interceptor.Interceptors;

/**
 *
 * @author martin
 */
@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class EchoServices {

    //@EJB
    //private EchoServicesLocalInterface echoServicesLocalInterface;


    public String echo() {
        //echoServicesLocalInterface.echo();
        return "EchoServices.echo";
    }
    
}
