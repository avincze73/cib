/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import com.cib.entity.Department;
import com.cib.server1.interceptors.LogInterceptor;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class DepartmentServices2 {

    @EJB
    private DepartmentRepository2 departmentRepository2;

    public List<Department> getAllDepartments() {
        return departmentRepository2.findAllDepartments();
    }

    public void save(Department department) throws MyException1 {
        departmentRepository2.saveDepartment(department);
        throw new MyException1("error from DepartmentServices2");
    }

}
