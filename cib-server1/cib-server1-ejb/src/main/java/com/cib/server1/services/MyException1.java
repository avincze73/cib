/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import javax.ejb.ApplicationException;

/**
 *
 * @author avincze
 */
@ApplicationException(rollback=true)
public class MyException1 extends Exception {

    /**
     * Creates a new instance of <code>MyException1</code> without detail
     * message.
     */
    public MyException1() {
    }

    /**
     * Constructs an instance of <code>MyException1</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public MyException1(String msg) {
        super(msg);
    }
}
