/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import com.cib.entity.Department;
import com.cib.intf.DepartmentServicesInterface;
import com.cib.server1.interceptors.LogInterceptor;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.List;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
@Local(DepartmentServicesInterface.class)
public class DepartmentServices3 {

    @Inject
    private DepartmentRepository departmentRepository;

    public void save(String name) {
        Department department = new Department();
        department.setName(name);
        departmentRepository.saveDepartment(department);
    }

}
