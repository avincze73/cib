/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import com.cib.entity.Department;
import com.cib.server1.interceptors.LogInterceptor;
import javassist.bytecode.ExceptionsAttribute;

import java.util.List;
import javax.ejb.*;
import javax.interceptor.Interceptors;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class DepartmentServices {

    @EJB
    private DepartmentRepository departmentRepository;

    public List<Department> getAllDepartments() {
        return departmentRepository.findAllDepartments();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(Department department) throws MyException1 {
        departmentRepository.saveDepartment(department);
        //throw new Exception("error from DepartmentServices");
    }

}
