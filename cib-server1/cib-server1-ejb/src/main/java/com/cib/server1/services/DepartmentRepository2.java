/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import com.cib.entity.Department;
import com.cib.server1.interceptors.LogInterceptor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class DepartmentRepository2 {

    @PersistenceContext(unitName = "cib-server1-pu2")
    private EntityManager em;
    
    public List<Department> findAllDepartments(){
        return em.createQuery("select d from Department d", Department.class).getResultList();
    }

    public void saveDepartment(Department department){
        if (department.getId() == null){
            em.persist(department);
        } else {
            em.merge(department);
        }
    }


}
