package com.cib.server1.services;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class ScheduledSingletonServices {

    @Schedule(second = "10", minute = "0/1", hour = "*", info = "ScheduledSingletonServices.echoToLog timer", persistent = false)
    public void echoToLog(){
        Logger.getLogger(getClass().getName()).log(Level.INFO, "ScheduledSingleton service is called at " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd hh:mm:ss")));
    }
}
