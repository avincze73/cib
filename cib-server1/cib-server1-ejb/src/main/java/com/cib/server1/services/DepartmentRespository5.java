package com.cib.server1.services;

import com.cib.entity.Department;
import com.cib.server1.producers.Cib2Database;
import com.cib.server1.producers.CibDatabase;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Stateless()
@LocalBean
public class DepartmentRespository5 {
    @Inject
    @CibDatabase
    EntityManager em;


    @Inject
    @Cib2Database
    EntityManager em2;

    public void add(Department department){
        em.persist(department);
    }
}
