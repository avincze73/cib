/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.interceptors;

import com.cib.server1.services.EchoServices;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author avincze
 */
public class LogInterceptor {

    @AroundInvoke
    public Object incerceptBusinessMethod(InvocationContext ctx) throws Exception {
        Logger.getLogger(getClass().getName()).log(Level.INFO, ctx.getMethod().getDeclaringClass().getCanonicalName() + "::" + ctx.getMethod().getName() + " is called.");
        return ctx.proceed();
    }
}
