/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.services;

import com.cib.server1.interceptors.LogInterceptor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.Interceptor;
import javax.interceptor.Interceptors;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class JndiServices {

    //@Resource
    //private Context context;

    public void dumpJndiToLog() {
        try {
            Context context = (Context)new InitialContext().lookup("java:comp/env");
            NamingEnumeration<NameClassPair> result = context.list("");
            while (result.hasMore()) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, result.next().getName());
            }
        } catch (NamingException ex) {
            Logger.getLogger(JndiServices.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
