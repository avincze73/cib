package com.cib.server1.services;

import com.cib.entity.Department;
import javassist.bytecode.ExceptionsAttribute;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless(name = "DepartmentServiceFacadeEJB")
@LocalBean
public class DepartmentServiceFacade {
    @Inject
    private DepartmentServices departmentServices;

    @Inject
    private DepartmentServices2 departmentServices2;

    public void insertDepartments() throws MyException1 {
        Department department = new Department();
        department.setName("department4");

        departmentServices.save(department);
        departmentServices2.save(department);
    }


}
