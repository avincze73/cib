package com.cib.server1.services;

import com.cib.server1.interceptors.LogInterceptor;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
@Interceptors(LogInterceptor.class)
public class JMSClientServices {

    @Resource(lookup = "jms/libertyQCF")
    private ConnectionFactory connectionFactory;
    @Resource(lookup = "jndi_INPUT_Q")
    private Queue queue;

    public void sendMessageToQueue() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "1. Sending message to jms queue");

        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(queue);
            messageProducer.setTimeToLive(300000);
            TextMessage message = session.createTextMessage();
            String text = "first message from jms client";
            message.setText(text);
            messageProducer.send(message);
            messageProducer.close();
            session.close();
            connection.close();
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Sent Message: " + text);
        } catch (JMSException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessageToQueue2() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "2. Sending message to jms queue");

        try (JMSContext context = connectionFactory.createContext()) {
            String text = "second message from jms client";
            context.createProducer().send(queue, text);
        }
    }
}
