package com.cib.server1.services;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class ScheduledServices {

    @Schedule(second = "30", minute = "0/1", hour = "*", info = "ScheduledServices.echoToLog timer", persistent = false)
    public void echoToLog() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "ScheduledSession30 service is called at " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd hh:mm:ss")));
    }

}
