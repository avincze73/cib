package com.cib.server1.services;

import com.cib.entity.Department;

import javax.annotation.Resource;
import javax.annotation.Resources;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

@Stateless
@LocalBean
public class DepartmentServices4 {

    @Inject
    private DepartmentRespository4 departmentRespository4;

    @Resource
    private SessionContext sessionContext;

    public void add(Department department){
        //departmentRespository4.add(department);
        DepartmentRespository4 departmentRespository4;
        departmentRespository4 = (DepartmentRespository4) sessionContext.lookup("com.cib.server1.services.DepartmentRespository4");
        departmentRespository4.add(department);


    }
}
