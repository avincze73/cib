package com.cib.server1.servlets;

import com.cib.server1.services.JMSClientServices;
import com.cib.server1.services.JMSClientServices2;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "JMSClientServlet2", urlPatterns = {"/jmsclientservlet2"})
public class JMSClientServlet2 extends HttpServlet {

    @Inject
    private JMSClientServices jmsClientServices;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        jmsClientServices.sendMessageToQueue2();
    }
}
