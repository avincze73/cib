package com.cib.server1.servlets;

import com.cib.server1.services.JMSClientServices;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "JMSClientServlet", urlPatterns = {"/jmsclientservlet"})
public class JMSClientServlet extends HttpServlet {

    @EJB
    private JMSClientServices jmsClientServices;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        jmsClientServices.sendMessageToQueue();
    }
}
