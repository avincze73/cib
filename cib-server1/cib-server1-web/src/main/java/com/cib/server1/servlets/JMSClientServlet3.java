package com.cib.server1.servlets;

import com.cib.server1.services.JMSClientServices2;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "JMSClientServlet3", urlPatterns = {"/jmsclientservlet3"})
public class JMSClientServlet3 extends HttpServlet {

    @Inject
    private JMSClientServices2 jmsClientServices2;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        jmsClientServices2.sendMessageToQueue();
    }
}
