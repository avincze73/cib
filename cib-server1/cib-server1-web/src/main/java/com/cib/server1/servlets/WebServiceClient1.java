/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.server1.servlets;

import com.cib.intf.DepartmentWebServiceBeanInterface;
import com.cib.server1.client.DepartmentWebServiceEJB;
import com.cib.server1.client.DepartmentWebServiceEJB_Service;
import com.cib.server1.services.DepartmentServiceFacade;
import com.cib.server1.services.MyException1;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author avincze
 */
@WebServlet(name = "WebServiceClient1", urlPatterns = {"/webserviceclient1"})
public class WebServiceClient1 extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/pvs-va_9080/cibservices/dws.wsdl")
    private DepartmentWebServiceEJB_Service service_1;

    
    //@WebServiceRef(type = DepartmentWebServiceBeanInterface.class)
    //DepartmentWebServiceBeanInterface service;


    

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet WebServiceClient1</title>");
            out.println("</head>");
            out.println("<body>");

            DepartmentWebServiceEJB port = service_1.getDepartmentWebServiceEJBPortName();
            port.echo();
            //service.echo();
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
