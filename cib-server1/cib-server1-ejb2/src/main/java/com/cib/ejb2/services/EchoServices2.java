/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cib.ejb2.services;

import com.cib.ejb2.interceptors.LogInterceptor;
import com.cib.intf.EchoServicesLocalInterface;

import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.interceptor.Interceptors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class EchoServices2 implements EchoServicesLocalInterface {

    public void echo() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "calling echo...");
    }


}
