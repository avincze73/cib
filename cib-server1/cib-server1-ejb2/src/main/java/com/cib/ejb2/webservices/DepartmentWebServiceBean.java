package com.cib.ejb2.webservices;

import com.cib.ejb2.interceptors.LogInterceptor;
import com.cib.intf.AddDepartmentRequest;
import com.cib.intf.AddDepartmentResponse;
import com.cib.intf.DepartmentServicesInterface;
import com.cib.intf.DepartmentWebServiceBeanInterface;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless
@Interceptors(LogInterceptor.class)
@WebService(name = "DepartmentWebServiceEJB", serviceName = "DepartmentWebServiceEJB", portName = "DepartmentWebServiceEJBPortName")
public class DepartmentWebServiceBean implements DepartmentWebServiceBeanInterface {

    @Inject
    private DepartmentServicesInterface departmentServices;

    @WebMethod
    public AddDepartmentResponse addDepartment(AddDepartmentRequest request){
        AddDepartmentResponse response = new AddDepartmentResponse();
        departmentServices.save(request.getName());
        return response;
    }

    @WebMethod
    public void echo(){

    }
}
