package com.cib;

import com.cib.wsclient.AddDepartment;
import com.cib.wsclient.AddDepartmentInputMessage;
import com.cib.wsclient.DepartmentWebServiceEJB;
import com.cib.wsclient.DepartmentWebServiceEJB_Service;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        DepartmentWebServiceEJB_Service client = new DepartmentWebServiceEJB_Service();
        DepartmentWebServiceEJB service =  client.getDepartmentWebServiceEJBPortName();
        AddDepartment addDepartment = new AddDepartment();
        AddDepartmentInputMessage inputMessage = new AddDepartmentInputMessage();
        inputMessage.setName("dep1");
        AddDepartment addDepartment1 = new AddDepartment();
        addDepartment1.setArg0(inputMessage);
        service.addDepartment(addDepartment1);
        System.out.println( "Hello World!" );
    }
}
