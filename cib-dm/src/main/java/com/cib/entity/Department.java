package com.cib.entity;

import com.cib.entity.CibEntity;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Department
 *
 */
@Entity
public class Department extends CibEntity {

	
	private String name;
	private static final long serialVersionUID = 1L;

	public Department() {
		super();
	}   
	public Department(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
